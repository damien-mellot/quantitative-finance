'''
Settings for the Capital Asset Pricing Model in Classical Financial Theory
'''
import numpy as np
from sympy import *
init_printing(use_unicode=False, use_latex=False)

# The probability measure
probability_third = np.ones(3) / 3

# The prices of stock and bond today
stock_t0 = 10
bond_t0 = 10

# The uncertain payoff of the stock and bond tomorrow
stock_t1 = np.array((20, 10, 5))
bond_t1 = np.array((1, 12, 13))

# The market price vector
market_t0 = np.array((stock_t0, bond_t0))

# The market payoff matrix
market_t1 = np.array((stock_t1, bond_t1)).T

# Return vector of the risky asset
return_stock = (stock_t1 / stock_t0) - 1

# Return vector of the risk-less asset
return_bond = (bond_t1 / bond_t0) - 1

# Return matrix for the traded assets
return_market = (market_t1 / market_t0) - 1

# market portfolio weights
phi_market = np.array((0.8, 0.2))

# risk-less asset
risk_less_asset = 0.0025

# constraints and bounds
constraints = [{'type': 'eq', 'fun': lambda phi: np.sum(phi) - 1},
               {'type': 'eq', 'fun': lambda phi: expected_return_phi(phi) - target}]
bounds = ((0,1), (0,1))

# Defines SymPy symbols
mu, sigma, b, v = symbols('mu sigma b v')

# Solves the utility function for mu
sol = solve('mu - b / 2 * (sigma ** 2 + mu ** 2) -v', mu)

# Subsitutes numerical values for b,v
u1 = sol[0].subs({'b': 1, 'v': 0.1})
u2 = sol[0].subs({'b': 1, 'v': 0.125})

# Generate callable functions from the resulting equations
f1 = lambdify(sigma, u1)
f2 = lambdify(sigma, u2)

# Specifies valus for sigma over which to evaluate the functions
sigma_ = np.linspace(0.0, 0.5)

# Evauates the callable functions for the two different utility levels
u1_ = f1(sigma_)
u2_ = f2(sigma_)


# Specifies the risk-less short rate
r = 0.005

# Defines the market portfolio
market = '.SPX'

sym = 'AAPL.O'

