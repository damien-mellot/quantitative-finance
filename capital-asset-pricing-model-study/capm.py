import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import minimize
from sympy import *
init_printing(use_unicode=False, use_latex=False)
import pandas as pd

import settings_capm as stg

class CAPM:
    
    def __init__(self):
        
        '''class initialization
        '''
        
    def capm_representation(self):
        
        plt.figure(figsize=(12,6))
        plt.plot((0,0.3), (0.01, 0.22), label='capital market line')
        plt.plot(0, 0.01, 'o', ms=9, label='risk-less asset')
        plt.plot(0.2, 0.15, '^', ms=9, label='market portfolio')
        plt.annotate('$(0, \\bar{r})$', (0, 0.01), (-0.01, 0.02))
        plt.annotate('$(\sigma_M, \mu_M)$', (0.2, 0.15), (0.19, 0.16))
        plt.xlabel('expected volatility')
        plt.ylabel('expected return')
        plt.legend();
        
    def expected_return(self, return_X):
        
        return np.dot(stg.probability_third, return_X)
    
    def variance(self, return_X):
    
        return ((return_X - self.expected_return(stg.probability_third, return_X))**2).mean()

    def volatility(self, return_X):
        
        return np.sqrt(self.variance(stg.probability_third, return_X))
    
    def covariance(self, return_X):
        
        return np.cov(return_X.T, aweights=stg.probability_third, ddof=0)
    
    
    
    def expected_return_phi(self, phi):
    
        return np.dot(phi, self.expected_return(stg.return_market))
    

    def variance_phi(self, phi):
        
        cov = np.cov(stg.return_market.T, aweights=stg.probability_third, ddof=0)
        
        return np.dot(phi, np.dot(cov, phi))
    
    def volatility_phi(self, phi):
        
        return self.variance_phi(phi)**0.5
    
    
    def minimize_volatility(self):
        
        return minimize(self.volatility_phi,
                        (0.5,0.5), 
                        constraints=stg.constraints[0], 
                        bounds=stg.bounds)
    
    
    def optimal_portfolio(self):
        
        
        constraints = [{'type': 'eq', 'fun': lambda phi: np.sum(phi) - 1},
                       {'type': 'eq', 'fun': lambda phi: self.expected_return_phi(phi) - target}]
        
        # Generates the set of target expected returns
        targets = np.linspace(self.expected_return_phi(self.minimize_volatility()['x']), 0.16)
        
        # Derives the minimum volatility portfolio given a target expected return
        frontier = []
        for target in targets:
            phi_eff = minimize(self.volatility_phi, (0.5,0.5),
                               constraints=constraints, 
                               bounds=stg.bounds)['x']
            frontier.append((self.volatility_phi(phi_eff),
                         self.expected_return_phi(phi_eff)))
        frontier = np.array(frontier)
        
        plt.figure(figsize=(12,6))
        plt.plot(frontier[:, 0], frontier[:, 1], 'm.', ms=5,
                 label='efficient frontier')
        plt.plot(0, stg.risk_less_asset, 'o', ms=9, label='risk-less asset')
        plt.plot((0, 0.6), 
                 (stg.risk_less_asset, stg.risk_less_asset + ((self.expected_return_phi(stg.phi_market) - stg.risk_less_asset) / self.volatility_phi(stg.phi_market))* 0.6),
                  'r', 
                  label='capital market line', 
                  lw=2.0)
        plt.annotate('$(0, \\bar{r})$', (0, stg.risk_less_asset), (-0.015, stg.risk_less_asset + 0.01))
        plt.annotate('$(\sigma_M, \mu_M)$', (self.volatility_phi(stg.phi_market), self.expected_return_phi(stg.phi_market)),
                     (self.volatility_phi(stg.phi_market) - 0.025, self.expected_return_phi(stg.phi_market) + 0.01))
        plt.xlabel('expected volatility')
        plt.ylabel('expected return')
        plt.legend();
        
        
    # The utility function in risk-return space, b=1
    def utility_function(self, p):
        mu, sigma = p
        return mu - 1 / 2 * (sigma**2 + mu**2)
    
    
    def maximize_utility(self):
        
        # The condition that the portfolio be on the CML
        cons = {'type': 'eq',
                'fun': lambda p: p[0] - (stg.risk_less_asset + (self.expected_return_phi(stg.phi_market) - stg.risk_less_asset) / self.volatility_phi(stg.phi_market)*p[1])}
        
        opt = minimize(lambda p: -self.utility_function(p), 
               (0.1, 0.3),
               constraints=cons)
        
        return opt
    
    
    def indifference_curves(self):
        
        plt.figure(figsize=(12,6))
        plt.plot(stg.sigma_, stg.u1_, label='$v=0.1$')
        plt.plot(stg.sigma_, stg.u2_, '--', label='$v=0.1255$')
        plt.xlabel('expected volatility')
        plt.ylabel('expected return')
        plt.legend();
        
    
    def optimal_utility_level_portfolio(self):
        
        # Defines the indifferent curve for the optimal utility level
        u = stg.sol[0].subs({'b': 1, 'v': -self.maximize_utility()['fun']})
        
        # Derives numerical values to plot the indifference curve
        f = lambdify(stg.sigma, u)
        u_ = f(stg.sigma_)
        
        plt.figure(figsize=(12,6))
        plt.plot(0, stg.risk_less_asset, 'o', ms=9, label='risk-less asset')
        plt.plot(self.volatility_phi(stg.phi_market), self.expected_return_phi(stg.phi_market), '^', ms=9, label='market portfolio')
        plt.plot(self.maximize_utility()['x'][1], self.maximize_utility()['x'][0], 'v', ms=9, label='optimal portfolio')
        plt.plot((0, 0.5), (stg.risk_less_asset, stg.risk_less_asset + (self.expected_return_phi(stg.phi_market) - stg.risk_less_asset) / self.volatility_phi(stg.phi_market)*0.5),
                            label='capital market line', lw=2.0)
        plt.plot(stg.sigma_, u_, '--', label='f$v={}$'.format(-round(self.maximize_utility()['fun'], 3)))
        plt.xlabel('expected volatility')
        plt.ylabel('expected return')
        plt.legend();
        
        
    def capm_portfolio_stats(self):
        
        df = pd.read_csv('capm_data.csv', index_col=0, parse_dates=True)
        
        rets = np.log(df/df.shift(1)).dropna()
        res = pd.DataFrame()
        
        # 1 : Derives the beta of the stock
        # 2 : Calculates the expected return given previous year's beta and current year market portfolio performance
        # 3 : Calculates the realized performance of the stock for the current year
        # 4 : Collects and prints all the results
        for sym in rets.columns[:4]:
            print('\n' + sym)
            print(54 * '=')
            for year in range(2010, 2019):
                rets_ = rets.loc[f'{year}-01-01':f'{year}-12-31']
                muM = rets_[stg.market].mean() * 252
                cov = rets_.cov().loc[sym, stg.market] # 1
                var = rets_[stg.market].var()
                beta = cov / var
                rets_ = rets.loc[f'{year + 1}-01-01':f'{year + 1}-12-31']
                muM = rets_[stg.market].mean() * 252
                mu_capm = stg.r + beta * (muM - stg.r) # 2
                mu_real = rets_[sym].mean() # 3
                res = res.append(pd.DataFrame({'symbol': sym,
                                               'mu_capm': mu_capm,
                                               'mu_real': mu_real},
                                           index=[year + 1]),
                                        sort=True) # 4
                print(f'{year + 1} | beta: {beta} | mu_capm: {mu_capm} | mu_real: {mu_real}') # 4
                
                
    def expected_vs_realized_returns(self):
        
        df = pd.read_csv('capm_data.csv', index_col=0, parse_dates=True)
        
        rets = np.log(df/df.shift(1)).dropna()
        res = pd.DataFrame()
        
        for sym in rets.columns[:4]:
            for year in range(2010, 2019):
                rets_ = rets.loc[f'{year}-01-01':f'{year}-12-31']
                muM = rets_[stg.market].mean() * 252
                cov = rets_.cov().loc[sym, stg.market] # 1
                var = rets_[stg.market].var()
                beta = cov / var
                rets_ = rets.loc[f'{year + 1}-01-01':f'{year + 1}-12-31']
                muM = rets_[stg.market].mean() * 252
                mu_capm = stg.r + beta * (muM - stg.r) # 2
                mu_real = rets_[sym].mean() # 3
                res = res.append(pd.DataFrame({'symbol': sym,
                                               'mu_capm': mu_capm,
                                               'mu_real': mu_real},
                                               index=[year + 1]),
                                        sort=True) # 4
                
        return res
    
    
    def correlation_expected_realized(self):
        
        return self.expected_vs_realized_returns()[self.expected_vs_realized_returns()['symbol'] == stg.sym].corr()
    
    def plot_expected_vs_realized(self):
        
        self.expected_vs_realized_returns()[self.expected_vs_realized_returns()['symbol'] == stg.sym].plot(kind='bar', 
                                                                                                           figsize=(12,6), title=stg.sym);
    def average_values(self):
        
        grouped = self.expected_vs_realized_returns().groupby('symbol').mean()
        grouped.plot(kind='bar', figsize=(12,6), title='Average Values');
        
        