import numpy as np
from scipy.optimize import minimize
from pylab import plt, mpl

import settings_mvpt as stg

class MarkovitzPortfolio:
    
    '''
    Methods and graphs to calculate the best Mean-Variance portfolio given probabilities and assets distribution
    '''
    
    
    def __init__(self):
        
        '''
        class initialization
        '''
    
    def expected_return(self, probability, return_X):
        
        return np.dot(probability, return_X)
    
    def variance(self, probability, return_X):
        
        return ((return_X - self.expected_return(probability, return_X))**2).mean()
    
    def volatility(self, probability, return_X):
        
        return np.sqrt(self.variance(probability, return_X))
    
    def covariance(self, probability, return_X):
        
        return np.cov(return_X.T, aweights=probability, ddof=0)
    
    
    # The portfolio expected return
    def expected_return_phi(self, probability, return_X, phi):
        
        return np.dot(phi, self.expected_return(probability, return_X))
    
    # The portfolio expected return
    def expected_return_phi_to_maximize(self, phi):
        
        return np.dot(phi, self.expected_return(stg.probability_third, stg.return_matrix_2))
                      
                      
    def variance_phi(self, probability, return_X, phi):
        
        cov = np.cov(return_X.T, aweights=probability, ddof=0)
        
        return np.dot(phi, np.dot(cov, phi))
    
   
    # The portfolio expected volatility
    def volatility_phi(self, probability, return_X, phi):
        
        return self.variance_phi(probability, return_X, phi)**0.5
    
    
    # The portfolio expected volatility used for minimization
    def volatility_phi_to_minimize(self, phi):
                
        return self.variance_phi(stg.probability_third, stg.return_matrix_2, phi)**0.5
    
    # Minimizes the expected portfolio volatility
    def minimize_volatility(self):
        
        return minimize(self.volatility_phi_to_minimize,
                        (0.5,0.5), 
                        constraints=stg.constraints, 
                        bounds=stg.bounds)
    
    # Defines the sharpe ratio function , assuming a short rate of 0
    def sharpe(self, probability, return_X, phi):
        
        return self.expected_return_phi(probability, return_X, phi) / self.volatility_phi(probability, return_X, phi)
    
    # The sharpe ration function for maximization
    def sharpe_to_maximize(self, phi):
        
        return self.expected_return_phi(stg.probability_third, stg.return_matrix_2, phi) / self.volatility_phi(stg.probability_third, stg.return_matrix_2, phi)
    
    # Maximizes the Sharpe ratio by minimizing its negative value
    def maximize_sharpe(self):
        
        return minimize(lambda phi: -self.sharpe_to_maximize(phi), 
                        (0.5,0.5), 
                        constraints=stg.constraints, 
                        bounds=stg.bounds)
    
    
     # graphic representation
    def investment_opportunity_set(self, probability, return_X):
        
        plt.style.use('seaborn')
        mpl.rcParams['savefig.dpi'] = 300
        mpl.rcParams['font.family'] = 'serif'
        
        # Random portfolio compositions, normalized to 1
        phi_mcs = np.random.random((2, 200))
        phi_mcs = (phi_mcs / phi_mcs.sum(axis=0)).T
        
        # Expected portfolio volatility and return for the random compositions
        mcs = np.array([(self.volatility_phi(probability, return_X, phi), self.expected_return_phi(probability, return_X, phi)) for phi in phi_mcs])
        
        plt.figure(figsize=(12,6))
        plt.plot(mcs[:, 0], mcs[:, 1], 'bo')
        plt.xlabel('expected volatility')
        plt.ylabel('expected return');
        
    def min_vol_max_sharpe_portfolios(self):
        
        plt.style.use('seaborn')
        mpl.rcParams['savefig.dpi'] = 300
        mpl.rcParams['font.family'] = 'serif'
        
        # Random portfolio compositions, normalized to 1
        phi_mcs = np.random.random((2, 200))
        phi_mcs = (phi_mcs / phi_mcs.sum(axis=0)).T
        
        mcs = np.array([(self.volatility_phi_to_minimize(phi), self.expected_return_phi_to_maximize(phi)) for phi in phi_mcs])
        
        plt.figure(figsize=(10,6))
        plt.plot(mcs[:, 0], mcs[:, 1], 'ro', ms=5)
        plt.plot(self.volatility_phi_to_minimize(self.minimize_volatility()['x']), 
                 self.expected_return_phi_to_maximize(self.minimize_volatility()['x']), 
                 '^', 
                 ms=12.5, 
                 label='minimum volatility')
        plt.plot(self.volatility_phi_to_minimize(self.maximize_sharpe()['x']), 
                 self.expected_return_phi_to_maximize(self.maximize_sharpe()['x']),
                 'v', ms=12.5, label='maximum Sharpe ratio')
        plt.xlabel('expected volatility')
        plt.ylabel('expected return')
        plt.legend();
        
        
    def efficient_frontier(self):
        
        # The new constraint fixes a target level for the expected return
        constraints = [{'type': 'eq', 'fun': lambda phi: np.sum(phi) - 1},
                       {'type': 'eq', 'fun': lambda phi: self.expected_return_phi_to_maximize(phi) - target}]
        
        bounds = ((0,1), (0,1))
        
        # Generates the set of target expected returns
        targets = np.linspace(self.expected_return_phi_to_maximize(self.minimize_volatility()['x']), 0.16)
        
        # Derives the minimum volatility portfolio given a target expected return
        frontier = []
        for target in targets:
            phi_eff = minimize(self.volatility_phi_to_minimize, (0.5,0.5),
                       constraints=constraints, bounds=bounds)['x']
            frontier.append((self.volatility_phi_to_minimize(phi_eff),
                             self.expected_return_phi_to_maximize(phi_eff)))
        frontier = np.array(frontier)
        
        plt.figure(figsize=(10,6))
        plt.plot(frontier[:, 0], frontier[:, 1], 'mo', ms=5,
                 label='efficient frontier')
        plt.plot(self.volatility_phi_to_minimize(self.minimize_volatility()['x']), 
                 self.expected_return_phi_to_maximize(self.minimize_volatility()['x']),
                 '^', 
                 ms=12.5, 
                 label='minimum volatility')
        plt.plot(self.volatility_phi_to_minimize(self.maximize_sharpe()['x']), 
                 self.expected_return_phi_to_maximize(self.maximize_sharpe()['x']),
                 'v', 
                 ms=12.5, 
                 label='maximum Sharpe ratio')
        plt.xlabel('expected volatility')
        plt.ylabel('expected return')
        plt.legend;
                        


    