'''
Settings for the Mean-Variance Markovitz Portfolio in Classical Financial Theory
'''

import numpy as np
from scipy.optimize import minimize

# The probability measure
probability_half = np.array((0.5, 0.5))
probability_third = np.ones(3) / 3

# The prices of stock and bond today
stock_t0 = 10
bond_t0 = 10

stockA_t0 = 10
stockB_t0 = 10

# The uncertain payoff of the stock and bond tomorrow
stock_t1 = np.array((20,5))
bond_t1 = np.array((11,11))

stockA_t1 = np.array((20,10,5))
stockB_t1 = np.array((1,12,13))

# The market price vector
market_t0 = np.array((stock_t0,bond_t0))

market_t0_2 = np.array((stockA_t0, stockB_t0))

# The market payoff matrix
market_t1 = np.array((stock_t1,bond_t1)).T

market_t1_2 = np.array((stockA_t1,stockB_t1)).T

# Return vector of the risky asset
return_stock = (stock_t1 / stock_t0) - 1

# Return vector of the risk-less asset
return_bond = (bond_t1 / bond_t0) - 1

# Return matrix for the traded assets
return_matrix = (market_t1 / market_t0) - 1

return_matrix_2 = (market_t1_2 / market_t0_2) - 1

# Portfolio weights
phi = np.array([0.5, 0.5])

# constraints and bounds
constraints = {'type': 'eq', 'fun': lambda phi: np.sum(phi) - 1}
bounds = ((0,1), (0,1))
