import numpy as np

symbols = ['AAPL.O', 'MSFT.O', 'INTC.O', 'AMZN.O', 'GLD']

weights = [0.2, 0.2, 0.2, 0.2, 0.2]

# Specifies the bounds for the single asset weights
bnds = len(symbols)*[(0,1),]

# Specifies that all weights need to add up to 100%
cons = {'type': 'eq', 'fun': lambda weights: np.sum(weights) - 1}
